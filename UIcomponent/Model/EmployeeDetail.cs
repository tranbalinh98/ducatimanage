﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIcomponent.Model
{
    class EmployeeDetail
    {

        private Employee _Employee;
        public Employee Employee { get => _Employee; set { _Employee = value; } }

        private RoleUser _RoleUser;
        public RoleUser RoleUser { get => _RoleUser;set { _RoleUser = value; } }
    }
}
