﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIcomponent.Model
{
    public class VehicleDetail
    {
        public Vehicle Vehicle { get; set; }
        public String ProducerName { get; set; }
    }
}
