﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UIcomponent.View;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using UIcomponent.Common;
using UIcomponent.Ultil;


namespace UIcomponent.ViewModel
{
    class LoginViewModel : BaseViewModel
    {
        //global 
        //LoginForm loginForm;

        public ICommand LoginComand { get; set; }
        public ICommand ExitComand { get; set; }
        public ICommand PasswordChangedCommand { get; }

        public bool IsLogin { get; set; }
        private String _UserName;
        private String _Password;
         
        public String userName { get => _UserName; set { _UserName = value;OnPropertyChanged(); } }
        public String password { get => _Password; set { _Password = value;OnPropertyChanged(); } }

        MainWindow main;

        public LoginViewModel()
        {
           

            LoginComand = new RelayCommand<ThemedWindow>((p) => { return true; }, p =>
            {
                Login(p);
            });

            ExitComand = new RelayCommand<ThemedWindow>((p) => { return p == null ? false : true; }, p =>
            {
                MessageBoxResult isClose = MessageBox.Show(MessageUltil.WARNING_CLOSE, "CẢNH BÁO", MessageBoxButton.YesNo,MessageBoxImage.Warning);
                if(isClose == MessageBoxResult.Yes)
                {
                    if(main != null)
                    {
                        main.Close();
                    }
                    p.Close();
                }
                else
                {
                    return;
                }
                
            });

            PasswordChangedCommand = new RelayCommand<PasswordBox>((p) => { return true; }, p =>
            {
                password = p.Password;
            });


        }
        private void Login(ThemedWindow w)
        {
            if (w == null)
            {
                return;
            }
            if (LoginUltil.validateLogin(_UserName, _Password))
            {
                w.Hide();
                main = new MainWindow();
                main.ShowDialog();
                w.Show();
            }
            else
            {
                MessageBox.Show(MessageUltil.ERROR_LOGIN,"CẢNH BÁO",MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
    }
}
