﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UIcomponent.View;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using System.Collections.ObjectModel;
using UIcomponent.Model;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;
using DevExpress.Xpf.Core.Native;
using UIcomponent.Ultil;

namespace UIcomponent.ViewModel
{
    class EmployeeViewModel : BaseViewModel
    {
        public ICommand Add { get; set; }
        public ICommand Edit { get; set; }
        public ICommand Delete { get; set; }
        public ICommand Refresh { get; set; }
        public ICommand Search { get; set; }
        public ICommand CheckGender { get; set; }


        /// <summary>
        /// List binding data
        /// </summary>

        private ObservableCollection<EmployeeDetail> _List;
        private ObservableCollection<RoleUser> _ListRole;

        public ObservableCollection<EmployeeDetail> List { get => _List; set { _List = value; OnPropertyChanged(); } }
        public ObservableCollection<RoleUser> ListRole { get => _ListRole; set { _ListRole = value; OnPropertyChanged(); } }

        /// <summary>
        /// DataBinding
        /// </summary>
        /// 
        private EmployeeDetail _SelectedItem;
        public EmployeeDetail SelectedItem { get => _SelectedItem; set { _SelectedItem = value; OnPropertyChanged(); onListChange(); } }

        private Employee employeeNew;

        private System.Guid _EmployeeID;
        private string _EmployeeCode;
        private string _EmployeeName;
        private Nullable<System.Guid> _RoleID;
        private string _IndentityCard;
        private Nullable<bool> _Gender;
        private Nullable<bool> _NotGender;
        private Nullable<double> _WorkDay;

        public System.Guid EmployeeID { get => _EmployeeID; set { _EmployeeID = value; OnPropertyChanged(); } }
        public string EmployeeCode { get => _EmployeeCode; set { _EmployeeCode = value; OnPropertyChanged(); } }
        public string EmployeeName { get => _EmployeeName; set { _EmployeeName = value; OnPropertyChanged(); } }
        public Nullable<System.Guid> RoleID { get => _RoleID; set { _RoleID = value; OnPropertyChanged(); } }
        public string IndentityCard { get => _IndentityCard; set { _IndentityCard = value; OnPropertyChanged(); } }
        public Nullable<bool> Gender { get => _Gender; set { _Gender = value; OnPropertyChanged(); } }
        public Nullable<bool> NotGender { get => _NotGender; set { _NotGender = value; OnPropertyChanged(); } }
        public Nullable<double> WorkDay { get => _WorkDay; set { _WorkDay = value; OnPropertyChanged(); } }
        /// <summary>
        /// 
        /// </summary>
        /// 
        private RoleUser _SelectedRole;
        public RoleUser SelectedRole { get => _SelectedRole; set { _SelectedRole = value; OnPropertyChanged(); onComboBoxChange(); } }

        private string _RoleCode;
        private string _RoleName;

        public string RoleCode { get => _RoleCode; set { _RoleCode = value; OnPropertyChanged(); } }
        public string Role { get => _RoleName; set { _RoleName = value; OnPropertyChanged(); } }
        /// <summary>
        /// 
        /// </summary>

        public EmployeeViewModel()
        {
            employeeNew = new Employee();
            ListRole = new ObservableCollection<RoleUser>(DataProvider.Ins.DB.RoleUsers);
            loadData();

            Add = new RelayCommand<object>((p) => {
                if (String.IsNullOrEmpty(EmployeeName))
                {
                    return false;
                }
                if (String.IsNullOrEmpty(EmployeeCode))
                {
                    return false;
                }
                //if (String.IsNullOrEmpty(EmployeeCode))
                //{
                //    return false;
                //}

                return true;
            }, p =>
            {
                addEmployee();
            });
            Edit = new RelayCommand<object>((p) => {
                if (String.IsNullOrEmpty(EmployeeName))
                {
                    return false;
                }
                var emp = DataProvider.Ins.DB.Employees.Where(x => x.EmployeeID == EmployeeID).FirstOrDefault();
                if (emp.EmployeeCode != EmployeeCode)
                {
                    return true;
                }
                if (emp.EmployeeName != EmployeeName)
                {
                    return true;
                }
                if (emp.Gender != Gender)
                {
                    return true;
                }
                if (emp.IndentityCard != IndentityCard)
                {
                    return true;
                }
                if (emp.RoleID != RoleID)
                {
                    return true;
                }
                return false;
            }, p =>
            {
                try
                {
                    Employee employee = DataProvider.Ins.DB.Employees.Where(x => x.EmployeeID == EmployeeID).FirstOrDefault();
                    employee.EmployeeName = EmployeeName;
                    employee.EmployeeCode = EmployeeCode;
                    employee.Gender = Gender;
                    employee.IndentityCard = IndentityCard;
                    employee.RoleID = RoleID;

                    DataProvider.Ins.DB.SaveChanges();
                    MessageBox.Show(MessageUltil.SUCCESS_UPDATE);
                    loadData();
                }
                catch(Exception e)
                {
                    MessageBox.Show(MessageUltil.ERROR_UPDATE);
                }
                


            });
            Delete = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBoxResult result = MessageBox.Show(MessageUltil.WARNING_DELETE,"CẢNH BÁO",MessageBoxButton.YesNo,MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        Employee employee = DataProvider.Ins.DB.Employees.Where(x => x.EmployeeID == EmployeeID).FirstOrDefault();
                        DataProvider.Ins.DB.Employees.Remove(employee);
                        DataProvider.Ins.DB.SaveChanges();
                        MessageBox.Show(MessageUltil.SUCCESS_DELETE);
                        loadData();
                    }
                    catch(Exception e)
                    {
                        MessageBox.Show(MessageUltil.ERROR_DELETE);
                    }
                }
            });
            Refresh = new RelayCommand<object>((p) => { return true; }, p =>
            {
                loadData();
            });
            Search = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add search");
            });
            CheckGender = new RelayCommand<object>((p) => { return true; }, p =>
            {
                
            });
        }
        void loadData()
        {
            List = new ObservableCollection<EmployeeDetail>();
            var Employees = DataProvider.Ins.DB.Employees;
            foreach(Employee item in Employees)
            {
                var roleUser = DataProvider.Ins.DB.RoleUsers.Where(x => x.RoleID == item.RoleID).FirstOrDefault();
                List.Add(new EmployeeDetail { Employee = item, RoleUser = roleUser });
            }
            resetText();
        }
        void onListChange()
        {
            if (SelectedItem != null)
            {
                EmployeeID = SelectedItem.Employee.EmployeeID;
                EmployeeCode = SelectedItem.Employee.EmployeeCode;
                EmployeeName = SelectedItem.Employee.EmployeeName;
                RoleID = SelectedItem.Employee.RoleID;
                Role = SelectedItem.RoleUser.RoleName;
                RoleCode = SelectedItem.RoleUser.RoleCode;
                Gender = SelectedItem.Employee.Gender;
                NotGender = !SelectedItem.Employee.Gender;
                IndentityCard = SelectedItem.Employee.IndentityCard;
                WorkDay = SelectedItem.Employee.WorkDay;
            }
            else
            {
                return;
            }
        }
        void onComboBoxChange()
        {
            RoleID = SelectedRole.RoleID;
            Role = SelectedRole.RoleName;
            RoleCode = SelectedRole.RoleCode;
        }
        void addEmployee()
        {
            Employee emp = new Employee();
            emp.EmployeeID = System.Guid.NewGuid();
            emp.EmployeeCode = EmployeeCode;
            emp.EmployeeName = EmployeeName;
            emp.Gender = Gender;
            emp.IndentityCard = IndentityCard;
            emp.RoleID = RoleID;
            emp.WorkDay = 0;
            int validate = Ultil.ValidateUltil.ValidateEmployee(emp);
            if (validate == 1)
            {
                MessageBox.Show(Ultil.MessageUltil.WARNING_EXIST_CODE);
                return;
            }
            if(validate == 2)
            {
                MessageBox.Show(Ultil.MessageUltil.WARNING_GENDER_NULL+emp.Gender);
                return;
            }
            try
            {
                DataProvider.Ins.DB.Employees.Add(emp);
                DataProvider.Ins.DB.SaveChanges();
                loadData();

                MessageBox.Show(Ultil.MessageUltil.SUCCESS_ADD);
            }catch(Exception e)
            {
                MessageBox.Show(Ultil.MessageUltil.ERROR_DELETE + e);
            }
            
        }
        void resetText()
        {
            EmployeeID = System.Guid.NewGuid();
            EmployeeCode = null;
            EmployeeName = null;
            RoleID = null;
            Role = null;
            RoleCode = null;
            Gender = false;
            NotGender = false;
            IndentityCard = null;
            WorkDay = 0;
        }
    }
}
