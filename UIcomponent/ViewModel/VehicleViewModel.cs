﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UIcomponent.View;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using UIcomponent.Model;
using System.Data.Common;
using System.Collections.ObjectModel;

namespace UIcomponent.ViewModel
{

    
    public class VehicleViewModel : BaseViewModel
    {

        public ObservableCollection<Vehicle> ListVehicle { get; set; }
        private Vehicle _SelectedItem;
        public Vehicle SelectedItem { get =>_SelectedItem; set { 
                _SelectedItem = value; OnPropertyChanged(); 
                if(SelectedItem != null)
                {
                    VehicleCode = SelectedItem.VehicleCode;
                    VehicleName = SelectedItem.VehicleName;
                    VehicleColor = SelectedItem.VehicleColor;
                    VehicleImage = SelectedItem.VehicleImage;
                    VehicleCost = SelectedItem.VehicleCost.ToString();
                    VehicleHP = SelectedItem.VehicleHP.ToString();
                    VehicleWeight = SelectedItem.VehicleWeight.ToString();
                    VehicleImage = SelectedItem.VehicleImage;
                    SetImagex = SelectedItem.VehicleImage;
                    ProducerID = SelectedItem.ProducerID;
                    //ProducerName = DataProvider.Ins.DB.Producers.Where(x => x.ProducerID == SelectedItem.ProducerID).FirstOrDefault().ProduceName;
                    ProducerName = SelectedItem.Producer.ProduceName;
                }
            } }
        private String _VehicleCode;
        public String VehicleCode { get => _VehicleCode; set { _VehicleCode = value; OnPropertyChanged(); } }
        private String _VehicleName;
        public String VehicleName { get=>_VehicleName; set { _VehicleName = value; OnPropertyChanged(); } }
        private String _VehicleColor;
        public String VehicleColor { get=>_VehicleColor; set { _VehicleColor = value; OnPropertyChanged(); } }
        private String _VehicleCost;
        public String VehicleCost { get=>_VehicleCost; set { _VehicleCost = value; OnPropertyChanged(); } }
        private String _ProducerName;
        public String ProducerName { get =>_ProducerName; set { _ProducerName = value; OnPropertyChanged(); } }
        private System.Guid _ProducerID;
        public System.Guid ProducerID { get =>_ProducerID; set { _ProducerID = value; OnPropertyChanged(); } }
        private String _VehicleID;
        public String VehicleID { get=>_VehicleID; set { _VehicleID = value; OnPropertyChanged(); } }
        private String _VehicleHP;
        public String VehicleHP { get=>_VehicleHP; set { _VehicleHP = value;OnPropertyChanged(); } }
        public String _VehicleImage;
        public String VehicleImage { get=>_VehicleImage; set { _VehicleImage = value; OnPropertyChanged(); } }

        private String _VehicleWeight;
        public String VehicleWeight { get => _VehicleWeight; set { _VehicleWeight = value; OnPropertyChanged(); } }

        public ICommand Add { get; set; }
        public ICommand Edit { get; set; }
        public ICommand Delete { get; set; }
        public ICommand Refresh { get; set; }
        public ICommand Search { get; set; }

        private String _SetImagex;
        public String SetImagex { 
            get => _SetImagex; set
            {
                _SetImagex = Ultil.Ultil.StartSourceImage+value;
                OnPropertyChanged();
            } }

        public VehicleViewModel()
        {
            //ListVehicle = new ObservableCollection<PROC_GET_VEHICLE_DETAIL_Result>( DataProvider.Ins.DB.PROC_GET_VEHICLE_DETAIL().ToList());
            ListVehicle = new ObservableCollection<Vehicle>( DataProvider.Ins.DB.Vehicles);

            Add = new RelayCommand<object>((p) => {
                if (String.IsNullOrEmpty(VehicleName))
                {
                    return false;
                }

                var VName = DataProvider.Ins.DB.Vehicles.Where(x => x.VehicleName == VehicleName);
                if(VName == null || VName.Count() != 0)
                {
                    return false;
                }
                return true;
                
            }
             , p =>{
                 var vehic = new Vehicle() {
                     VehicleID = System.Guid.NewGuid(),
                     VehicleCode = "V000" +( DataProvider.Ins.DB.Vehicles.Count()+1),
                     VehicleName = VehicleName,
                     VehicleCost = Convert.ToDecimal(VehicleCost),
                     VehicleWeight = Convert.ToInt32(VehicleWeight),
                     VehicleColor = VehicleColor,
                     VehicleHP = VehicleHP,
                     VehicleImage = VehicleImage,
                     ProducerID = ProducerID
                 };

                 try
                 {
                     DataProvider.Ins.DB.Vehicles.Add(vehic);
                     DataProvider.Ins.DB.SaveChanges();
                     ListVehicle.Add(vehic);
                     ResetText();
                     MessageBox.Show(Ultil.MessageUltil.SUCCESS_ADD);
                 }
                 catch(Exception e)
                 {
                     MessageBox.Show("Thêm thất bại, kiểm tra lại thông tin \n Hoặc kết nối"+ e.Message);
                 }
                
                 
             
             });
            Edit = new RelayCommand<object>((p) => {
                if (String.IsNullOrEmpty(VehicleName))
                {
                    return false;
                }

                var VName = DataProvider.Ins.DB.Vehicles.Where(x => x.VehicleName == VehicleName);
                if (VName == null || VName.Count() != 0)
                {
                    return false;
                }
                return true;
            }, p =>
            {
                var vehic = DataProvider.Ins.DB.Vehicles.Where(x => x.VehicleID == SelectedItem.VehicleID).SingleOrDefault();

                vehic.VehicleName = VehicleName;
                vehic.VehicleCost = Convert.ToDecimal(VehicleCost);
                vehic.VehicleWeight = Convert.ToInt32(VehicleWeight);
                vehic.VehicleColor = VehicleColor;
                vehic.VehicleHP = VehicleHP;
                vehic.VehicleImage = VehicleImage;
                vehic.ProducerID = ProducerID;


                try
                {
                    DataProvider.Ins.DB.SaveChanges();
                    SelectedItem.VehicleName = VehicleName;
                    SelectedItem.VehicleCost = Convert.ToDecimal(VehicleCost);
                    SelectedItem.VehicleWeight = Convert.ToInt32(VehicleWeight);
                    SelectedItem.VehicleColor = VehicleColor;
                    SelectedItem.VehicleHP = VehicleHP;
                    SelectedItem.VehicleImage = VehicleImage;
                    SelectedItem.ProducerID = ProducerID;

                    ResetText();
                    MessageBox.Show(Ultil.MessageUltil.SUCCESS_UPDATE);
                }
                catch (Exception e)
                {
                    MessageBox.Show(Ultil.MessageUltil.ERROR_UPDATE + e.Message);
                }
            });
            Delete = new RelayCommand<object>((p) => {
                return SelectedItem == null ? false : true;
            }, p =>
            {
                MessageBoxResult result = MessageBox.Show(Ultil.MessageUltil.WARNING_DELETE, "CẢNH BÁO", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    Vehicle vehicle = DataProvider.Ins.DB.Vehicles.Where(x => x.VehicleID == SelectedItem.VehicleID).FirstOrDefault();
                    DataProvider.Ins.DB.Vehicles.Remove(vehicle);
                    DataProvider.Ins.DB.SaveChanges();

                    ListVehicle.Remove(vehicle);
                    ResetText();
                    MessageBox.Show(Ultil.MessageUltil.SUCCESS_DELETE);
                }
                else
                {
                    return;
                }
            });
            Refresh = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add refresh");
            });
            Search = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add search");
            });
            

        }
       void ResetText()
        {
            SelectedItem = null;
            VehicleName = null;
            VehicleCost = null;
            VehicleWeight = null;
            VehicleColor = null;
            VehicleHP = null;
            VehicleImage = null;
            ProducerName = null;
        }
    }
}
