﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UIcomponent.View;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using System.Collections.ObjectModel;
using UIcomponent.Model;

namespace UIcomponent.ViewModel
{
    class CustomerViewModel : BaseViewModel
    {
        public ICommand Add { get; set; }
        public ICommand Edit { get; set; }
        public ICommand Delete { get; set; }
        public ICommand Refresh { get; set; }
        public ICommand Search { get; set; }

        private ObservableCollection<Customer> _ListCustomer;
        public ObservableCollection<Customer> ListCustomer { get => _ListCustomer; set { _ListCustomer = value; OnPropertyChanged(); } }

        private ObservableCollection<Vehicle> _ListVehicle;
        public ObservableCollection<Vehicle> ListVehicle { get => _ListVehicle; set { _ListVehicle = value; OnPropertyChanged(); } }

        private Customer _SelectedItem;
        public Customer SelectedItem { get => _SelectedItem; set { _SelectedItem = value; OnPropertyChanged();loadVehicle(); } }

        public System.Guid CustomerID { get => _CustomerID; set { _CustomerID = value; OnPropertyChanged(); } }
        public System.Guid _CustomerID;
        public string CustomerCode { get => _CustomerCode; set { _CustomerCode = value; OnPropertyChanged(); } }
        private string _CustomerCode;
        public string CustomerName { get => _CustomerName; set { _CustomerName = value; OnPropertyChanged(); } }
        private string _CustomerName;
        public Nullable<bool> CustomerType { get => _CustomerType; set { _CustomerType = value; OnPropertyChanged(); } }
        private Nullable<bool> _CustomerType;
        public string CustomerContact { get => _CustomerContact; set { _CustomerContact = value; OnPropertyChanged(); } }
        private string _CustomerContact;
        public string CustomerEmail { get => _CustomerEmail; set { _CustomerEmail = value; OnPropertyChanged(); } }
        public string _CustomerEmail;
        public CustomerViewModel()
        {
            ListCustomer = new ObservableCollection<Customer>(DataProvider.Ins.DB.Customers);

            Add = new RelayCommand<object>((p) => {
                if (String.IsNullOrEmpty(CustomerName))
                {
                    return false;
                }

                var VName = DataProvider.Ins.DB.Customers.Where(x => x.CustomerName == CustomerName);
                if (VName == null || VName.Count() != 0)
                {
                    return false;
                }
                return true;
            }, p =>
            {
                Customer customer = new Customer();

                customer.CustomerID = System.Guid.NewGuid();
                customer.CustomerCode = "CUS00" + (DataProvider.Ins.DB.Customers.Count() + 30);
                customer.CustomerName = SelectedItem.CustomerName;
                customer.CustomerType = SelectedItem.CustomerType;
                customer.CustomerContact = SelectedItem.CustomerContact;
                customer.CustomerEmail = SelectedItem.CustomerEmail;

                try
                {
                    DataProvider.Ins.DB.Customers.Add(customer);
                    DataProvider.Ins.DB.SaveChanges();
                    ListCustomer.Add(customer);
                    ClearText();
                    MessageBox.Show(Ultil.MessageUltil.SUCCESS_ADD);
                }
                catch (Exception e)
                {
                    MessageBox.Show(Ultil.MessageUltil.ERROR_ADD+e.Message);
                }
            });
            Edit = new RelayCommand<object>((p) => {
                if (String.IsNullOrEmpty(CustomerName))
                {
                    return false;
                }

                var VName = DataProvider.Ins.DB.Customers.Where(x => x.CustomerName == CustomerName);
                if (VName == null || VName.Count() != 0)
                {
                    return false;
                }
                return true;
            }, p =>
            {
                Customer customer = DataProvider.Ins.DB.Customers.Where(x => x.CustomerID == SelectedItem.CustomerID).FirstOrDefault();

                customer.CustomerID = SelectedItem.CustomerID;
                customer.CustomerName = SelectedItem.CustomerName;
                customer.CustomerType = SelectedItem.CustomerType;
                customer.CustomerContact = SelectedItem.CustomerContact;
                customer.CustomerEmail = SelectedItem.CustomerEmail;
                try
                {
                    DataProvider.Ins.DB.SaveChanges();
                    SelectedItem.CustomerCode = CustomerCode;
                    SelectedItem.CustomerName = CustomerName;
                    SelectedItem.CustomerType = CustomerType;
                    SelectedItem.CustomerEmail = CustomerEmail;
                    SelectedItem.CustomerContact = CustomerContact;

                    ClearText();
                    MessageBox.Show(Ultil.MessageUltil.SUCCESS_UPDATE);
                }
                catch (Exception e)
                {
                    MessageBox.Show(Ultil.MessageUltil.ERROR_UPDATE + e.Message);
                }
            });
            Delete = new RelayCommand<object>((p) => { return SelectedItem == null ? false:true ; }, p =>
            {
                MessageBoxResult result = MessageBox.Show(Ultil.MessageUltil.WARNING_DELETE, "CẢNH BÁO", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if(result == MessageBoxResult.Yes)
                {
                    Customer customer = DataProvider.Ins.DB.Customers.Where(x => x.CustomerID == SelectedItem.CustomerID).FirstOrDefault();
                    DataProvider.Ins.DB.Customers.Remove(customer);
                    DataProvider.Ins.DB.SaveChanges();

                    ListCustomer.Remove(customer);
                    ClearText();
                    MessageBox.Show(Ultil.MessageUltil.SUCCESS_DELETE);
                }
                else
                {
                    return;
                }
            });
            Refresh = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add refresh");
            });
            Search = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add search");
            });
        }

        void loadVehicle()
        {
            if (SelectedItem != null)
            {
                //ListVehicle = new ObservableCollection<Vehicle>(
                //        DataProvider.Ins.DB.Vehicles.Where(
                //            x => x.VoucherDetails == DataProvider.Ins.DB.VoucherDetails.Where(
                //                y => y.Voucher.Customer == DataProvider.Ins.DB.Customers.Where(z =>z.CustomerID == SelectedItem.CustomerID)
                //            )
                //        )
                //    ) ;
                CustomerCode = SelectedItem.CustomerCode;
                CustomerID = SelectedItem.CustomerID;
                CustomerName = SelectedItem.CustomerName;
                CustomerContact = SelectedItem.CustomerContact;
                CustomerEmail = SelectedItem.CustomerEmail;
                CustomerType = SelectedItem.CustomerType;
            }
         }
        void ClearText()
        {
            SelectedItem = null;
            CustomerCode = null;
            CustomerName = null ;
            CustomerContact =null;
            CustomerEmail = null;
            CustomerType = null;
        }
    }
}
