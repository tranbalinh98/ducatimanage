﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UIcomponent.View;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;

namespace UIcomponent.ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        //global 
        public ICommand VehicleManageCommand { get; set; }
        public ICommand InventoryManageCommand { get; set; }
        public ICommand CustomerManageCommand { get; set; }
        public ICommand ProducerManageCommand { get; set; }
        public ICommand EmployeeManageCommand { get; set; }
        public ICommand ExitCommand { get; set; }

        public MainViewModel()
        {
            VehicleManageCommand = new RelayCommand<StackPanel>((p) => { return p==null ? false: true; }, p =>
             {
                 p.Children.Clear();
                 VehicleManage view = new VehicleManage();
                 p.Children.Add(view);
             });
            InventoryManageCommand = new RelayCommand<StackPanel>((p) => { return p==null ? false: true; }, p =>
             {
                 p.Children.Clear();
                 VehicleManage vehicleManage = new VehicleManage();
                 p.Children.Add(vehicleManage);
             });
            CustomerManageCommand = new RelayCommand<StackPanel>((p) => { return p==null ? false: true; }, p =>
             {
                 p.Children.Clear();
                 CustomerManage view = new CustomerManage();
                 p.Children.Add(view);
             });
            ProducerManageCommand = new RelayCommand<StackPanel>((p) => { return p==null ? false: true; }, p =>
             {
                 p.Children.Clear();
                 VehicleManage vehicleManage = new VehicleManage();
                 p.Children.Add(vehicleManage);
             });
            EmployeeManageCommand = new RelayCommand<StackPanel>((p) => { return p==null ? false: true; }, p =>
             {
                 p.Children.Clear();
                 EmployeeManage view = new EmployeeManage();
                 p.Children.Add(view);
             });
            ExitCommand = new RelayCommand<ThemedWindow>((p) => { return true; }, p =>
            {
                MessageBoxResult result = MessageBox.Show("Bạn muốn đăng xuất ?", "", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if(result == MessageBoxResult.Yes)
                {
                    p.Close();
                }
                else
                {
                    return;
                }
            });
        }
    }
}
