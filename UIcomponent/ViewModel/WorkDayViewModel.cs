﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UIcomponent.View;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;

namespace UIcomponent.ViewModel
{
    class WorkDayViewModel
    {
        public ICommand Add { get; set; }
        public ICommand Edit { get; set; }
        public ICommand Delete { get; set; }
        public ICommand Refresh { get; set; }
        public ICommand Search { get; set; }

        public WorkDayViewModel()
        {
            Add = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add click");
            });
            Edit = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add edit");
            });
            Delete = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add delete");
            });
            Refresh = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add refresh");
            });
            Search = new RelayCommand<object>((p) => { return true; }, p =>
            {
                MessageBox.Show("add search");
            });
        }
    }
}
