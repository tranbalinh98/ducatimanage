﻿#pragma checksum "..\..\..\View\CustomerManage.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "1CC64CE4A4747E0F9BC860A43A6D93CD9F5D647A80121A7E15B828AB4AD44C4C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DevExpress.Core;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Core.ConditionalFormatting;
using DevExpress.Xpf.Core.DataSources;
using DevExpress.Xpf.Core.Serialization;
using DevExpress.Xpf.Core.ServerMode;
using DevExpress.Xpf.DXBinding;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Editors.DataPager;
using DevExpress.Xpf.Editors.DateNavigator;
using DevExpress.Xpf.Editors.ExpressionEditor;
using DevExpress.Xpf.Editors.Filtering;
using DevExpress.Xpf.Editors.Flyout;
using DevExpress.Xpf.Editors.Popups;
using DevExpress.Xpf.Editors.Popups.Calendar;
using DevExpress.Xpf.Editors.RangeControl;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Editors.Settings.Extension;
using DevExpress.Xpf.Editors.Validation;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Grid.ConditionalFormatting;
using DevExpress.Xpf.Grid.LookUp;
using DevExpress.Xpf.Grid.TreeList;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using UIcomponent.View;


namespace UIcomponent.View {
    
    
    /// <summary>
    /// CustomerManage
    /// </summary>
    public partial class CustomerManage : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView listCustomer;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView listVehicle;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Editors.TextEdit txtCustomerCode;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Editors.TextEdit txtCustomerContact;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Editors.TextEdit txtCustomerName;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Editors.TextEdit txtCustomerEmail;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Core.SimpleButton btnAdd;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Core.SimpleButton btnEdit;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Core.SimpleButton btnDelete;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Core.SimpleButton btnRefresh;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cboType;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\View\CustomerManage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Editors.TextEdit txtSearch;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/UIcomponent;component/view/customermanage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\CustomerManage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.listCustomer = ((System.Windows.Controls.ListView)(target));
            return;
            case 2:
            this.listVehicle = ((System.Windows.Controls.ListView)(target));
            return;
            case 3:
            this.txtCustomerCode = ((DevExpress.Xpf.Editors.TextEdit)(target));
            return;
            case 4:
            this.txtCustomerContact = ((DevExpress.Xpf.Editors.TextEdit)(target));
            return;
            case 5:
            this.txtCustomerName = ((DevExpress.Xpf.Editors.TextEdit)(target));
            return;
            case 6:
            this.txtCustomerEmail = ((DevExpress.Xpf.Editors.TextEdit)(target));
            return;
            case 7:
            this.btnAdd = ((DevExpress.Xpf.Core.SimpleButton)(target));
            return;
            case 8:
            this.btnEdit = ((DevExpress.Xpf.Core.SimpleButton)(target));
            return;
            case 9:
            this.btnDelete = ((DevExpress.Xpf.Core.SimpleButton)(target));
            return;
            case 10:
            this.btnRefresh = ((DevExpress.Xpf.Core.SimpleButton)(target));
            return;
            case 11:
            this.cboType = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 12:
            this.txtSearch = ((DevExpress.Xpf.Editors.TextEdit)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

