﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIcomponent.Ultil
{
    class MessageUltil
    {
        public const String DO_NOT_HAVE_ROW_SELECTED = "Chưa có xe nào được chọn để hiển thị, chỉnh sửa thông tin !";


        // success
        public const String SUCCESS_DELETE = "XÓA thành công !";
        public const String SUCCESS_UPDATE = "CẬP NHẬT thông tin thành công !";
        public const String SUCCESS_ADD = "THÊM MỚI thành công !";


        //error
        public const String ERROR_DELETE = "XÓA thất bại, xác nhận lại quyền truy cập hoặc thử lại !";
        public const String ERROR_UPDATE = "CẬP NHẬT thất bại, xác nhận lại quyền truy cập hoặc thử lại !";
        public const String ERROR_ADD = "THÊM MỚI thất bại, xác nhận lại quyền truy cập hoặc thử lại !";
        public const String ERROR_LOGIN = "ĐĂNG NHẬP thất bại.\n Tài khoản hoặc mật khẩu không chính xác !";


        //warning
        public const String WARNING_DELETE = "Bạn muốn XÓA ?\n Dữ liệu sẽ không được khôi phục";
        public const String WARNING_UPDATE = "Bạn muốn THAY ĐỔI ?\n Dữ liệu sẽ không được khôi phục";
        public const String WARNING_ADD = "Bạn muốn THÊM MỚI ?";
        public const String WARNING_CLOSE = "Bạn muốn Thoát chương trình ?";
        public const String WARNING_EXIST_CODE = "Mã đã tồn tại";
        public const String WARNING_GENDER_NULL = "Giới tính chưa được chọn";






    }
}
