﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIcomponent.Model;

namespace UIcomponent.Ultil
{
    public class ValidateUltil
    {
        public static int ValidateEmployee(Employee employee)
        {
            if (DataProvider.Ins.DB.Employees.Where(x => x.EmployeeCode == employee.EmployeeCode).Count()>0)
            {
                return 1;
            }
            if (employee.Gender == null)
            {
                return 2;
            }
            if(employee.WorkDay == null)
            {
                employee.WorkDay = 0;
            }
            if(employee.RoleID == null)
            {
                employee.RoleID = DataProvider.Ins.DB.RoleUsers.Where(x => x.RoleCode == "EMP").First().RoleID;
            }
            return 0;
        }


        public static bool isEditEmployee(Employee employeeOld, Employee employeeNew)
        {
            if(employeeOld == employeeNew)
            {
                return false;
            }
            return true;
        }
    }
}
